CREATE DATABASE IF NOT EXISTS unified_maps;

USE unified_maps;

CREATE TABLE map_type (
	`id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
	`type` VARCHAR(64) NOT NULL, 
	PRIMARY KEY (`id`), 
	UNIQUE (`type`)
) ENGINE=InnoDB Comment 'What type of map is being generated';

--
-- Source of how all map tables were generated, the structure and
-- the data source behind each one.  
--
CREATE TABLE maps (
	`id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
	`name` VARCHAR(64) NOT NULL, 
	`map_type_id` INT(11) unsigned NOT NULL, 
	`source_database` VARCHAR(64) NOT NULL, 
	`source_table` VARCHAR(64) NOT NULL, 
	`source_column_text` VARCHAR(64) NOT NULL, 
	`source_column_id` VARCHAR(64) NOT NULL, 
	`target_database` VARCHAR(64) NOT NULL, 
	`target_table` VARCHAR(64) NOT NULL, 
	`target_column_text` VARCHAR(64) NOT NULL, 
	`target_column_id` VARCHAR(64) NOT NULL, 
	`output_database` VARCHAR(64) NOT NULL, 
	`output_table` VARCHAR(64) NOT NULL, 
	`output_source_column_id` VARCHAR(64) NOT NULL, 
	`output_target_column_id` VARCHAR(64) NOT NULL, 
	`source_query` TEXT, 
	`target_query` TEXT, 
	`enabled` TINYINT(4) NOT NULL DEFAULT 0,
	`updated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
		ON UPDATE CURRENT_TIMESTAMP, 
	PRIMARY KEY (`id`), 
	UNIQUE (`name`), 
	FOREIGN KEY (`map_type_id`) 
		REFERENCES `map_type`(`id`)
		ON UPDATE CASCADE 
		ON DELETE RESTRICT
) ENGINE=InnoDB Comment 'The map structure and data sources';

--
-- A history of changes to the maps table for auditability.
-- updated by update_maps_history trigger
--
CREATE TABLE maps_history (
	`id` INT(11) unsigned NOT NULL, 
	`maps_id` INT(11) unsigned NOT NULL, 
	`name` VARCHAR(64) NOT NULL, 
	`map_type_id` INT(11) unsigned NOT NULL, 
	`source_database` VARCHAR(64) NOT NULL, 
	`source_table` VARCHAR(64) NOT NULL, 
	`source_column_text` VARCHAR(64) NOT NULL, 
	`source_column_id` VARCHAR(64) NOT NULL, 
	`target_database` VARCHAR(64) NOT NULL, 
	`target_table` VARCHAR(64) NOT NULL, 
	`target_column_text` VARCHAR(64) NOT NULL, 
	`target_column_id` VARCHAR(64) NOT NULL, 
	`output_database` VARCHAR(64) NOT NULL, 
	`output_table` VARCHAR(64) NOT NULL, 
	`output_source_column_id` VARCHAR(64) NOT NULL, 
	`output_target_column_id` VARCHAR(64) NOT NULL, 
	`source_query` TEXT, 
	`target_query` TEXT, 
	`enabled` TINYINT(4) NOT NULL,
	`updated` TIMESTAMP NULL,
	`created` TIMESTAMP NOT NULL DEFAULT NOW(), 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB Comment 'A history of the map structure and data sources';

--
-- All possible generation statuses
--
CREATE TABLE generated_status (
	`id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
	`status` VARCHAR(32) NOT NULL, 
	`description` VARCHAR(128), 
	`success` TINYINT(4) NOT NULL DEFAULT 0, 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB Comment 'The status of the job that last generated map output';

--
-- The maps that were generated and their status
--
CREATE TABLE maps_generated (
	`id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
	`maps_id` INT(11) unsigned NOT NULL, 
	`generated` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	`generated_status_id` INT(11) unsigned NOT NULL, 
	PRIMARY KEY (`id`), 
	FOREIGN KEY (`generated_status_id`) 
		REFERENCES `generated_status`(`id`) 
		ON UPDATE CASCADE 
		ON DELETE RESTRICT
) ENGINE=InnoDB Comment 'When the map last generated an output';


