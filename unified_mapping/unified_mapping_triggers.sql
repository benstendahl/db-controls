USE unified_maps;

-- Automaticaly populate the maps_history table
DELIMITER //
	CREATE TRIGGER `update_maps_history`
	BEFORE UPDATE ON `maps` 
		FOR EACH ROW 
		BEGIN
			INSERT INTO `maps_history`
			(
				`maps_id`, 
				`name`, 
				`map_type_id`, 
				`source_database`, 
				`source_table`, 
				`source_column_text`, 
				`source_column_id`, 
				`target_database`, 
				`target_table`, 
				`target_column_text`, 
				`target_column_id`, 
				`output_database`, 
				`output_table`, 
				`output_source_column_id`, 
				`output_target_column_id`, 
				`source_query`, 
				`target_query`, 
				`enabled`, 
				`updated`
			)
			VALUES
			(
				OLD.`id`, 
				OLD.`name`, 
				OLD.`map_type_id`, 
				OLD.`source_database`, 
				OLD.`source_table`, 
				OLD.`source_column_text`, 
				OLD.`source_column_id`, 
				OLD.`target_database`, 
				OLD.`target_table`, 
				OLD.`target_column_text`, 
				OLD.`target_column_id`, 
				OLD.`output_database`, 
				OLD.`output_source_column_id`, 
				OLD.`output_target_column_id`, 
				OLD.`source_query`, 
				OLD.`target_query`, 
				OLD.`enabled`, 
				OLD.`updated`
			);
		END;
	//

DELIMITER ;
