USE unified_maps;

--
-- Map Types, can be added through UI
-- 
INSERT INTO map_type
(
	id, 
	`type`
) VALUES (
	1, 
	'GPC Mapping'
);

INSERT INTO map_type
(
	id, 
	`type`
) VALUES (
	2, 
	'Brand/Manufacturer'
);


-- 
-- Generation Statuses, not editable
--
INSERT INTO generated_status 
(
	id, 
	status, 
	description, 
	success
) VALUES (
	1, 
	'SUCCESS', 
	'Successfully created a map table and populated with data', 
	1
);

INSERT INTO generated_status 
(
	id, 
	status, 
	description, 
	success
) VALUES (
	2, 
	'TEMP TABLE ONLY', 
	'Successfully created a temporary table, unable to make permanent', 
	0
);

INSERT INTO generated_status 
(
	id, 
	status, 
	description, 
	success
) VALUES (
	3, 
	'FAILURE', 
	'Unable to create the map table', 
	0
);


