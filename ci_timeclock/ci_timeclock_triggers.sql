USE ci_timeclock;

-- Automatically populate the punch history when changing an accountable record
DELIMITER //
	CREATE TRIGGER `update_punches_history` BEFORE UPDATE ON `punches`
		FOR EACH ROW 
		BEGIN
			INSERT INTO `punches_history`
			(
				`punches_id`,
				`users_id`, 
				`time`, 
				`approved`, 
				`approved_users_id`, 
				`note`, 
				`action`
			) VALUES
			(
				OLD.`id`, 
				OLD.`users_id`, 
				OLD.`time`, 
				OLD.`approved`, 
				OLD.`approved_users_id`, 
				OLD.`note`,
				'update'
			);
		END;
	//


	CREATE TRIGGER `delete_punches_history` BEFORE DELETE ON `punches`
		FOR EACH ROW 
		BEGIN
			INSERT INTO `punches_history`
			(
				`punches_id`,
				`users_id`, 
				`time`, 
				`approved`, 
				`approved_users_id`, 
				`note`,
				`action`
			) VALUES
			(
				OLD.`id`, 
				OLD.`users_id`, 
				OLD.`time`, 
				OLD.`approved`, 
				OLD.`approved_users_id`, 
				OLD.`note`, 
				'delete'
			);
		END;
	//
DELIMITER ;
