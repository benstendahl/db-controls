CREATE DATABASE IF NOT EXISTS ci_timeclock;

USE ci_timeclock;

CREATE TABLE config (
	`id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
	`name` VARCHAR(128) NOT NULL, 
	`value` VARCHAR(128) NULL, 
	`description` TEXT, 
	PRIMARY KEY (`id`), 
	UNIQUE (`name`)
) ENGINE=InnoDB COMMENT 'Config values specific to the database and application';

CREATE TABLE employee_type (
       `id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
       `type` VARCHAR(64) NOT NULL, 
       PRIMARY KEY (`id`), 
	   UNIQUE (`type`)
) ENGINE=InnoDB COMMENT 'Employee classification for pay periods. ex. salary, hourly';

 -- users table also defines supervisor relationship with a foreign key to itself
CREATE TABLE users (
		`id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
		`first_name` VARCHAR(63) NOT NULL, 
		`last_name` VARCHAR(63) NOT NULL, 
		`employee_type_id` INT(11) unsigned NOT NULL,
		`supervisor_users_id` INT(11) unsigned DEFAULT NULL,
		`display` TINYINT(4) NOT NULL DEFAULT 1, 
		PRIMARY KEY(`id`), 
		FOREIGN KEY (`employee_type_id`) 
			REFERENCES employee_type(id) 
			ON UPDATE CASCADE 
			ON DELETE RESTRICT, 
		FOREIGN KEY (`supervisor_users_id`) 
			REFERENCES users(id) 
			ON UPDATE CASCADE
			ON DELETE RESTRICT, 
) ENGINE=InnoDB COMMENT 'Users available for the timeclock application';

CREATE TABLE punches (  
       `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,      
       `users_id` INT(11) unsigned NOT NULL,   
       `time` TIMESTAMP NOT NULL DEFAULT NOW(),    
       `approved` TINYINT(4) NOT NULL DEFAULT 1,   
       `approved_users_id` INT(11) unsigned,   
       `note` TEXT,    
	   PRIMARY KEY (`id`), 
	   FOREIGN KEY (`users_id`) 
			REFERENCES users(id) 
			ON UPDATE CASCADE 
			ON DELETE RESTRICT, 
		FOREIGN KEY (`approved_users_id`) 
			REFERENCES users(id)
			ON UPDATE CASCADE
			ON DELETE RESTRICT
) ENGINE=InnoDB COMMENT 'Accountable record of punches for time keeping';


 -- Populated by trigger
CREATE TABLE punches_history (
       `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
       `punches_id` INT(11) unsigned NOT NULL,
	   `users_id` INT(11) unsigned NULL,
       `time` TIMESTAMP NULL,
       `approved` TINYINT(4) NOT NULL,   
       `approved_users_id` INT(11) unsigned,   
       `note` TEXT,
       `changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	   `action` VARCHAR(32) NOT NULL, 
       PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT 'Historic version of punches for auditability';

 -- Paid Time Off, Bereavement, Holiday, Unpaid Time Off, Maternity, etc. 
CREATE TABLE leave_type (
       `id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
       `type` VARCHAR(64) NOT NULL, 
       PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT 'Types of leave. ex. Bereavement, Paid Time Off, etc.';

 -- What leave types are available to each employee type and which 
 -- are visible to the employees of that type for selection 
CREATE TABLE employee_leave_type (
       `id` INT(11) unsigned NOT NULL AUTO_INCREMENT, 
       `employee_type_id` INT(11) unsigned NOT NULL, 
       `leave_type_id` INT(11) unsigned NOT NULL, 
       `visible` TINYINT(4) NOT NULL DEFAULT 0, 
       `available` TINYINT(4) NOT NULL DEFAULT 0, 
       PRIMARY KEY (`id`), 
       FOREIGN KEY (`employee_type_id`) 
			REFERENCES employee_type(id)
			ON UPDATE CASCADE 
			ON DELETE RESTRICT, 
		FOREIGN KEY (`leave_type_id`)
			REFERENCES leave_type(id) 
			ON UPDATE CASCADE 
			ON DELETE RESTRICT
) ENGINE=InnoDB COMMENT 'Which employees have which leave available and visible.  xref table';

CREATE TABLE employee_leave_request (
       `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
       `users_id` INT(11) unsigned NOT NULL, 
       `hours` INT(11) NOT NULL, 
       `start_date` TIMESTAMP NULL, 
       `end_date` TIMESTAMP NULL, 
       `requested` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
       PRIMARY KEY (`id`) 
) ENGINE=InnoDB COMMENT 'Leave requests from employees, not an accountable record of leave.';

CREATE TABLE employee_leave (
       `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
       `users_id` INT(11) unsigned NOT NULL, 
       `users_id_approved` INT(11) unsigned NOT NULL, 
       `hours` INT(11) NOT NULL, 
       `start_date` TIMESTAMP NULL, 
       `end_date` TIMESTAMP NULL, 
       PRIMARY KEY (`id`), 
       FOREIGN KEY (`users_id`) 
			REFERENCES users(id) 
			ON UPDATE CASCADE 
			ON DELETE RESTRICT, 
		FOREIGN KEY (`users_id_approved`) 
			REFERENCES users(id)
			ON UPDATE CASCADE 
			ON DELETE RESTRICT
) ENGINE=InnoDB COMMENT 'Accountable record of leave for time keeping.';

CREATE TABLE pay_period (
       `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
       `start_date` DATE NOT NULL, 
       `length` VARCHAR(32) NOT NULL, 
       `employee_type_id` INT(11) unsigned NOT NULL, 
       PRIMARY KEY (`id`), 
       FOREIGN KEY (`employee_type_id`) 
			REFERENCES employee_type(id) 
			ON UPDATE CASCADE 
			ON DELETE RESTRICT
) ENGINE=InnoDB COMMENT 'Pay periods defined per employee type';
