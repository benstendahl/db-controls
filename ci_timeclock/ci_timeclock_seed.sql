USE ci_timeclock;

 -- Default values
INSERT INTO config (`name`, `value`, `description`) VALUES ('version', '1', 'Version of the database');

INSERT INTO employee_type (`id`, `type`) VALUES (1, 'SALARY');
INSERT INTO employee_type (`id`, `type`) VALUES (2, 'HOURLY');

INSERT INTO leave_type (`id`, `type`) VALUES (1, 'PAID TIME OFF');
INSERT INTO leave_type (`id`, `type`) VALUES (2, 'UNPAID TIME OFF');
INSERT INTO leave_type (`id`, `type`) VALUES (3, 'JURY DUTY');
INSERT INTO leave_type (`id`, `type`) VALUES (4, 'BEREAVEMENT LEAVE');
INSERT INTO leave_type (`id`, `type`) VALUES (5, 'MATERNITY LEAVE');
INSERT INTO leave_type (`id`, `type`) VALUES (6, 'DISABILITY');
INSERT INTO leave_type (`id`, `type`) VALUES (7, 'HOLIDAY');

INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (1, 1, 1, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (1, 2, 0, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (1, 3, 0, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (1, 4, 0, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (1, 5, 0, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (1, 6, 0, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (1, 7, 0, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (2, 1, 1, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (2, 2, 0, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (2, 3, 0, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (2, 4, 0, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (2, 5, 0, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (2, 6, 0, 1);
INSERT INTO employee_leave_type (`employee_type_id`, `leave_type_id`, `visible`, `available`) VALUES (2, 7, 0, 1);
