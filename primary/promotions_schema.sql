USE primary;

CREATE TABLE promotion_types (
	`id` INT(11) unsigned NOT NULL auto_increment,
	`description` TEXT NOT NULL, 
	`operator` VARCHAR(4) NOT NULL default '*' COMMENT 'Mathmatical operator to apply to this promotion type', 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE promotions (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`campaign` VARCHAR(255) NOT NULL,
	`description` TEXT NULL, 
	`promotion_types_id` INT(11) unsigned NOT NULL, 
	`value` VARCHAR(4) NOT NULL COMMENT 'The value to which we need to apply the promotion_types.operator', 
	`start_date` TIMESTAMP NULL, 
	`end_date` TIMESTAMP NULL, 
	PRIMARY KEY (`id`), 
	FOREIGN KEY (`promotion_types_id`)
		REFERENCES `promotion_types`(`id`)
		ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB;

CREATE TABLE promotion_codes (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`code` VARCHAR(128) NOT NULL, 
	`promotions_id` INT(11) unsigned NOT NULL, 
	`start_date` TIMESTAMP NULL COMMENT 'Override value for particular promotion codes that are outside the bounds of a promotion', 
	`end_date` TIMESTAMP NULL COMMENT 'Override value for particular promotion codes that are outside the bounds of a promotion', 
	PRIMARY KEY (`id`),
	UNIQUE(`code`), 
	FOREIGN KEY (`promotions_id`)
		REFERENCES `promotions`(`id`)
		ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB;

CREATE TABLE reward_points (
	`id` INT(11) unsigned NOT NULL auto_increment,
	`points` INT(11) NOT NULL DEFAULT 0,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	`updated` TIMESTAMP NULL, 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;
