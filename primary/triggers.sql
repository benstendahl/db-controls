USE primary;

DELIMITER //
	-- Populates the history table so we have an automated record of activity
	CREATE TRIGGER `update_reward_points_history BEFORE UPDATE ON `reward_points`
		FOR EACH ROW
		BEGIN
			INSERT INTO `reward_points_history` 
			(
				`reward_points_id`, 
				`created`, 
				`updated`, 
				`action`
			) VALUES
			(
				OLD.`id`, 
				OLD.`created`, 
				OLD.`updated`, 
				'update'
			);
		END;
	//

	CREATE TRIGGER `delete_reward_points_history` BEFORE DELETE ON `reward_points`
		FOR EACH ROW
		BEGIN
			INSERT INTO `reward_points_history` 
			(
				`reward_points_id`, 
				`created`, 
				`updated`, 
				`action`
			) VALUES
			(
				OLD.`id`, 
				OLD.`created`, 
				OLD.`updated`, 
				'delete'
			);
		END;
	//

DELIMITER ;
