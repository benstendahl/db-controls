USE primary;

-----------------
--  Customers  --
-----------------
CREATE TABLE address (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`address_1` VARCHAR(255) NOT NULL, 
	`address_2` VARCHAR(128) NULL, 
	`address_3` VARCHAR(128) NULL, 
	`city` VARCHAR(255) NULL, 
	`state` VARCHAR(3) NULL,
	`zip` VARCHAR(32) NULL, 
	`country` VARCHAR(128) NOT NULL DEFAULT 'USA', 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE customer (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`email` VARCHAR(255) NOT NULL, 
	`first_name` VARCHAR(128) NULL, 
	`last_name` VARCHAR(128) NULL,
	`company` VARCHAR(128) NULL COMMENT 'If legal organization, doing business as (DBA), etc', 
	`phone_number` VARCHAR(32) NULL, 
	`billing_address_id` INT(11) unsigned NULL, 
	`mailing_address_id` INT(11) unsigned NULL, 
	`birth_month` TINYINT(4) NULL COMMENT 'The 2-digit month', 
	`birth_day` TINYINT(4) NULL COMMENT 'The 2-digit day', 
	`accepts_marketing` TINYINT(4) NOT NULL DEFAULT '1' COMMENT 'Do they want to receive our marketing communications',
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	`updated` TIMESTAMP NULL, 
	PRIMARY KEY (`id`), 
	FOREIGN KEY (`billing_address_id`) 
		REFERENCES `address`(`id`) 
		ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB;


---------------------
--  Reward points  --
---------------------
CREATE TABLE reward_action (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`action` VARCHAR(64) NOT NULL COMMENT 'The action taken that generated a credit or debit from the point total', 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE reward_ledger (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`customer_id` INT(11) unsigned NOT NULL, 
	`reward_action_id` INT(11) unsigned NOT NULL, 
	`amount` INT(11) NOT NULL, 
	`note` TEXT NULL, 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE reward_points (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`points` INT(11) NOT NULL DEFAULT '0',
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	`updated` TIMESTAMP NULL,
	PRIMARY KEY (`id`) 
) ENGINE=InnoDB;

CREATE TABLE reward_points_history (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`reward_points_id` INT(11) unsigned NOT NULL, 
	`reward_action_id` INT(11) unsigned NOT NULL, 
	`created` TIMESTAMP NULL, 
	`updated` TIMESTAMP NULL, 
	`changed` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`action` VARCHAR(16) NOT NULL, 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB COMMENT 'This holds the individual entries to reward points by recording the changes to the reward_points table';


CREATE TABLE customer_reward_points_xref (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`customer_id` INT(11) unsigned NOT NULL, 
	`reward_points_id` INT(11) unsigned NOT NULL, 
	PRIMARY KEY (`id`), 
	FOREIGN KEY (`customer_id`) 
		REFERENCES `customer`(`id`)
		ON UPDATE CASCADE ON DELETE RESTRICT, 
	FOREIGN KEY (`reward_points_id`) 
		REFERENCES `reward_points`(`id`) 
		ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB;

------------------
--  Promotions  --
------------------
CREATE TABLE promotion_type (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`description` TEXT NOT NULL, 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE promotion (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`campaign` VARCHAR(255) NOT NULL, 
	`description` TEXT NULL, 
	`promotion_type_id` INT(11) unsigned NOT NULL, 
	`value` VARCHAR(12) NOT NULL COMMENT 'The value to which we need to apply the promotion amount', 
	`start_date` TIMESTAMP NULL, 
	`end_date` TIMESTAMP NULL, 
	PRIMARY KEY(`id`)
	FOREIGN KEY `promotion_type_id` 
		REFERENCES `promotion_type`(`id`)
		ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB;

CREATE TABLE promotion_code (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`code` VARCHAR(128) NOT NULL, 
	`promotion_id` INT(11) unsigned NOT NULL, 
	`start_date` TIMESTAMP NULL COMMENT 'Override value for particular promotion codes that are outside the bounds of a promotion', 
	`end_date` TIMESTAMP NULL COMMENT 'Override value for particular promotion codes that are outside the bounds of a promotion',
	`redeemed` TIMESTAMP NULL COMMENT 'The date this promotion_code was redeemed, and no longer active',  
	PRIMARY KEY (`id`), 
	UNIQUE (`code`), 
	FOREIGN KEY (`promotion_id`) 
		REFERENCES `promotion`(`id`) 
		ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB;

CREATE TABLE customer_promotion_code_xref (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`promotion_code_id` INT(11) unsigned NOT NULL, 
	`customer_id` INT(11) unsigned NOT NULL,
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	PRIMARY KEY (`id`), 
	FOREIGN KEY (`promotion_code_id`) 
		REFERENCES `promotion_code`(`id`) 
		ON UPDATE CASCADE ON DELETE RESTRICT, 
	FOREIGN KEY (`customer_id`) 
		REFERENCES `customer`(`id`) 
		ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB;


--------------
--  Orders  --
--------------
CREATE TABLE payment_type (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`type` VARCHAR(64) NOT NULL, 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE sales_order (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`record_id` VARCHAR(64) NOT NULL COMMENT 'This is the ID for the original record of this sales order in Mongo', 
	`channel_order_id` VARCHAR(128) NOT NULL COMMENT 'Id for channel order reference, string for alpha-numeric', 
	`sub_total`	INT(11) NOT NULL, 
	`shipping` INT(11) NOT NULL, 
	`tax` INT(11) NOT NULL, 
	`credits` INT(11) NOT NULL COMMENT 'See sales_order_credit_xref, sales_order_promotions_xref, sales_order_item_promotions_xref', 
	`total` INT(11) NOT NULL, 
	`customer_id` INT(11) unsigned NOT NULL, 
	`promotion_code` VARCHAR(128) NULL COMMENT 'This can be used for a join but is not a true FK in case of missing promo codes', 
	`payment_type` VARCHAR(
) ENGINE=InnoDB;
