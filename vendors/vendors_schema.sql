CREATE DATABASE IF NOT EXISTS vendor_inventory_feeds;

USE vendor_inventory_feeds;

CREATE TABLE vendor_config (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`vendor_id` INT(11) unsigned NOT NULL,
	`feed_frequency` INT(11) NOT NULL DEFAULT 86400 COMMENT 'Number of seconds to check for a new inventory file', 
	`last_feed_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`notes` TEXT NULL COMMENT 'Any special information that needs to be know about this configuration',
	PRIMARY KEY(`id`), 
	UNIQUE(`vendor_id`)
) ENGINE=InnoDB;

-- 1, File Name, Look for this specific file name
-- 2, Latest By Extension, Look for the latest file with this extension
CREATE TABLE mask_type (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`name` VARCHAR(32) NOT NULL, 
	`description` VARCHAR(255) NULL, 
	PRIMARY KEY(`id`)
) ENGINE=InnoDB;

-- 1, (S)FTP, ftp, File Transfer Protocol
-- 2, HTTP(S), http, Hypertext Transfer Protocol
-- 3, EDI, multi, Electronic Data Interchange
-- 4, SSH, ssh, Secure Shell
CREATE TABLE feed_type (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`name` VARCHAR(128) NOT NULL,
	`protocol` VARCHAR(16) NOT NULL, 
	`description` VARCHAR(255) NULL, 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE csv_config (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`delimiter` VARCHAR(6) NULL, 
	`enclosed_by` VARCHAR(6) NULL, 
	`escaped_by` VARCHAR(6) NULL, 
	PRIMARY KEY(`id`)
) ENGINE=InnoDB;

-- redoing this so all configs can fit in one table
-- and all references can be normalized
CREATE TABLE feed_config (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`feed_type_id` INT(11) unsigned NOT NULL, 
	`url` TEXT NOT NULL, 
	`username` VARCHAR(64) NULL, 
	`password` VARCHAR(64) NULL,
	`path` VARCHAR(255) NULL COMMENT 'The path where the file(s) live on the server', 
	`mask_type_id` INT(11) unsigned NOT NULL COMMENT 'What type of value is expected in the mask column', 
	`mask` VARCHAR(128) NULL COMMENT 'Usually the file name or file extension', 
	`save_file_as` VARCHAR(128) NULL COMMENT 'Save the file as this name after pulling it onto our server', 
	`delete_file` TINYINT(4) NOT NULL DEFAULT 0 COMMENT 'Whether or not to attempt to delete the file after download', 
	`csv_config_id` INT(11) unsigned NULL COMMENT 'If the file is csv format, what are the configuration', 
	`notes` TEXT NULL COMMENT 'Any special information that needs to be known about this configuration', 
	PRIMARY KEY (`id`), 
	FOREIGN KEY (`feed_type_id`) 
		REFERENCES `feed_type`(`id`)
		ON UPDATE CASCADE ON DELETE RESTRICT, 
	FOREIGN KEY (`mask_type_id`)
		REFERENCES `mask_type`(`id`)
		ON UPDATE CASCADE ON DELETE RESTRICT, 
	FOREIGN KEY (`csv_config_id`) 
		REFERENCES `csv_config`(`id`)
		ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB;

CREATE TABLE vendor_feeds (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`vendor_config_id` INT(11) unsigned NOT NULL, 
	`feed_config_id` INT(11) unsigned NOT NULL, 
	PRIMARY KEY (`id`), 
	UNIQUE (`vendor_config_id`), 
	FOREIGN KEY (`vendor_config_id`)
		REFERENCES `vendor_config`(`id`)
		ON UPDATE CASCADE ON DELETE RESTRICT, 
	FOREIGN KEY (`feed_config_id`)
		REFERENCES `feed_config`(`id`)
		ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB;

-- 1, file
-- 2, object
CREATE TABLE object_type (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`name` VARCHAR(32) NOT NULL,
	`extension` VARCHAR(12) NULL COMMENT 'The extension of the file if it is a file type', 
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE feed_register (
	`id` INT(11) unsigned NOT NULL auto_increment, 
	`s3_bucket` VARCHAR(128) NOT NULL, 
	`s3_key` VARCHAR(128) NOT NULL,
	`s3_etag` VARCHAR(128) NULL, 
	`object_size` INT(64) NULL, 
	`object_type_id` INT(11) unsigned NOT NULL,
	`feed_type_name` VARCHAR(32) NULL, 
	`created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	`expires` TIMESTAMP NULL,
	PRIMARY KEY (`id`), 
	FOREIGN KEY (`object_type_id`)
		REFERENCES `object_type`(`id`)
		ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE=InnoDB;
