
--
-- Orgill
--
INSERT INTO vendor_config
	(`id`, `vendor_id`, `feed_frequency`, `last_feed_date`, `notes`)
VALUES
	(1, 3206, 86400, CURRENT_TIMESTAMP, '');

INSERT INTO feed_config
(
	`id`,
	`feed_type_id`, 
	`url`, 
	`username`, 
	`password`, 
	`path`, 
	`mask_type_id`, 
	`mask`, 
	`save_file_as`, 
	`delete_file`, 
	`csv_config_id`, 
	`notes`
) VALUES (
	1, 
	1, 
	'ftp.orgill.com', 
	'orgftp', 
	'YA5KDT', 
	'/orfillftp/webfiles/', 
	1, 
	'WEB_INVENTORY.TXT', 
	null, 
	0, 
	1, 
	''
);

INSERT INTO vendor_feeds
	(vendor_config_id, feed_config_id)
VALUES
	(1, 1);

--
-- American Mills
--
INSERT INTO vendor_config
	(`id`, `vendor_id`, `feed_frequency`, `last_feed_date`, `notes`)
VALUES
	(3, 3233, 86400, CURRENT_TIMESTAMP, '');

INSERT INTO feed_config
(
	`id`, 
	`feed_type_id`, 
	`url`, 
	`username`, 
	`password`, 
	`path`, 
	`mask_type_id`, 
	`mask`, 
	`save_file_as`, 
	`delete_file`, 
	`csv_config_id`, 
	`notes`
)
VALUES (
	3, 
	1, -- check this
	'feeds.toolking.com', 
	'americanmills', 
	'7T8VWXcE', 
	'inventory/', 
	2, 
	'.csv', 
	'inventory.csv', 
	0, 
	1, 
	''
);

INSERT INTO vendor_feeds
	(vendor_config_id, feed_config_id)
VALUES
	(3, 3);

-- 
-- Mobile Asset Solutions
--
INSERT INTO vendor_config
	(`id`, `vendor_id`, `feed_frequency`, `last_feed_date`, `notes`)
VALUES
	(2, 3523, 86400, CURRENT_TIMESTAMP, '');

INSERT INTO feed_config
(
	`id`, 
	`feed_type_id`, 
	`url`, 
	`username`, 
	`password`, 
	`path`, 
	`mask_type_id`, 
	`mask`, 
	`save_file_as`, 
	`delete_file`, 
	`csv_config_id`, 
	`notes`
)
VALUES (
	2, 
	3, -- check this
	'www.mobileassetsolutions.com', 
	null, 
	null, 
	null, 
	1, 
	'tk-inv.html', 
	'tk-inv.csv', 
	0, 
	1, 
	''
);

INSERT INTO vendor_feeds
	(vendor_config_id, feed_config_id)
VALUES
	(2, 2);

