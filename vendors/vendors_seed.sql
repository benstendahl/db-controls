USE vendor_inventory_feeds;


-- 
-- Feed Type 
-- 
INSERT INTO feed_type
	(`id`, `name`, `protocol`, `description`)
VALUES
	(1, 'FTP', 'ftp', 'File Trasfer Protocol');


INSERT INTO feed_type
	(`id`, `name`, `protocol`, `description`)
VALUES
	(2, 'SFTP', 'sftp', 'Secure File Trasfer Protocol');


INSERT INTO feed_type
	(`id`, `name`, `protocol`, `description`)
VALUES
	(3, 'HTTP', 'http', 'Hypertext Trasfer Protocol');


INSERT INTO feed_type
	(`id`, `name`, `protocol`, `description`)
VALUES
	(4, 'HTTPS', 'https', 'Secure Hypertext Trasfer Protocol');


INSERT INTO feed_type
	(`id`, `name`, `protocol`, `description`)
VALUES
	(5, 'EDI', 'edi', 'Electronic Data Interchange');


INSERT INTO feed_type
	(`id`, `name`, `protocol`, `description`)
VALUES
	(6, 'SSH', 'ssh', 'Secure Shell');

INSERT INTO feed_type
	(`id`, `name`, `protocol`, `description`)
VALUES
	(7, 'IMAP', 'imap', 'Internet Message Access Protocol');


-- 
-- Mask Type
--
INSERT INTO mask_type
	(`id`, `name`, `description`)
VALUES 
	(1, 'File Name', 'Exactly this filename');

INSERT INTO mask_type
	(`id`, `name`, `description`)
VALUES
	(2, 'Latest By Extension', 'The latest file in the directory with this extension');

INSERT INTO mask_type
	(`id`, `name`, `description`)
VALUES
	(3, 'From Email Address', 'The vendor feed file is designated by the \'from\' email address');

-- 
-- Ojbect Type 
--
INSERT INTO object_type
	(`id`, `name`, `extension`)
VALUES
	(1, 'text/plain', 'txt');
	
INSERT INTO object_type
	(`id`, `name`, `extension`)
VALUES 
	(2, 'text/csv', 'csv');

INSERT INTO object_type
	(`id`, `name`, `extension`)
VALUES
	(3, 'application/xml', 'xml');

INSERT INTO object_type
	(`id`, `name`, `extension`)
VALUES
	(4, 'application/vnd.ms-excel', 'xls');

-- 
-- Common CSV configs
--
INSERT INTO csv_config
	(`id`, `delimiter`, `enclosed_by`, `escaped_by`)
VALUES
	(1, ',', null, null);

INSERT INTO csv_config
	(`id`, `delimiter`, `enclosed_by`, `escaped_by`)
VALUES
	(2, '|', null, null);
